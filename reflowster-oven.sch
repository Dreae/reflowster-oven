EESchema Schematic File Version 4
LIBS:reflowster-oven-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ReflowsterOven Controller"
Date "2019-01-06"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Sensor_Temperature:MAX31855KASA U4
U 1 1 5C2E5B73
P 7200 3800
F 0 "U4" H 7200 3222 50  0000 C CNN
F 1 "MAX31855KASA" H 7200 3313 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8200 3450 50  0001 C CIN
F 3 "http://datasheets.maximintegrated.com/en/ds/MAX31855.pdf" H 7200 3800 50  0001 C CNN
	1    7200 3800
	-1   0    0    1   
$EndComp
$Comp
L reflowster-oven-rescue:ATmega32U2-AU-MCU_Microchip_ATmega U1
U 1 1 5C2E5CC1
P 3900 4700
F 0 "U1" H 3900 3214 50  0000 C CNN
F 1 "ATmega32U2-AU" H 3900 3123 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 3900 4700 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc7799.pdf" H 3900 4700 50  0001 C CNN
	1    3900 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3700 6550 3700
Wire Wire Line
	6550 3700 6550 3600
Wire Wire Line
	6550 3600 6350 3600
Wire Wire Line
	6800 3900 6550 3900
Wire Wire Line
	6550 3900 6550 3800
Wire Wire Line
	6550 3800 6350 3800
$Comp
L Logic_LevelTranslator:TXB0104D U3
U 1 1 5C2E63A5
P 5950 3900
F 0 "U3" H 5950 3022 50  0000 C CNN
F 1 "TXB0104D" H 5950 3113 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5950 3150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/txb0104.pdf" H 6060 3995 50  0001 C CNN
	1    5950 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 4000 6800 4000
$Comp
L power:+5V #PWR0101
U 1 1 5C2E6706
P 5850 4600
F 0 "#PWR0101" H 5850 4450 50  0001 C CNN
F 1 "+5V" H 5865 4773 50  0000 C CNN
F 2 "" H 5850 4600 50  0001 C CNN
F 3 "" H 5850 4600 50  0001 C CNN
	1    5850 4600
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5C2E678B
P 6050 4600
F 0 "#PWR0102" H 6050 4450 50  0001 C CNN
F 1 "+3.3V" H 6065 4773 50  0000 C CNN
F 2 "" H 6050 4600 50  0001 C CNN
F 3 "" H 6050 4600 50  0001 C CNN
	1    6050 4600
	-1   0    0    1   
$EndComp
$Comp
L Regulator_Linear:LP2985-3.3 U2
U 1 1 5C2E6841
P 3950 2250
F 0 "U2" H 3950 2592 50  0000 C CNN
F 1 "LP2985-3.3" H 3950 2501 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3950 2575 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lp2985.pdf" H 3950 2250 50  0001 C CNN
	1    3950 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2250 3300 2150
$Comp
L power:+5V #PWR0103
U 1 1 5C2E6A8A
P 3300 2150
F 0 "#PWR0103" H 3300 2000 50  0001 C CNN
F 1 "+5V" V 3315 2278 50  0000 L CNN
F 2 "" H 3300 2150 50  0001 C CNN
F 3 "" H 3300 2150 50  0001 C CNN
	1    3300 2150
	0    -1   -1   0   
$EndComp
Connection ~ 3300 2150
$Comp
L Device:C_Small C5
U 1 1 5C2E6B42
P 3300 2050
F 0 "C5" H 3392 2096 50  0000 L CNN
F 1 "1uF" H 3392 2005 50  0000 L CNN
F 2 "" H 3300 2050 50  0001 C CNN
F 3 "~" H 3300 2050 50  0001 C CNN
	1    3300 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C2E6BCE
P 3300 1950
F 0 "#PWR0104" H 3300 1700 50  0001 C CNN
F 1 "GND" H 3305 1777 50  0000 C CNN
F 2 "" H 3300 1950 50  0001 C CNN
F 3 "" H 3300 1950 50  0001 C CNN
	1    3300 1950
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5C2E6C09
P 3950 2550
F 0 "#PWR0105" H 3950 2300 50  0001 C CNN
F 1 "GND" H 3955 2377 50  0000 C CNN
F 2 "" H 3950 2550 50  0001 C CNN
F 3 "" H 3950 2550 50  0001 C CNN
	1    3950 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2150 3550 2150
Wire Wire Line
	3300 2250 3550 2250
$Comp
L power:+3.3V #PWR0106
U 1 1 5C2E6E6F
P 4600 2150
F 0 "#PWR0106" H 4600 2000 50  0001 C CNN
F 1 "+3.3V" V 4615 2278 50  0000 L CNN
F 2 "" H 4600 2150 50  0001 C CNN
F 3 "" H 4600 2150 50  0001 C CNN
	1    4600 2150
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5C2E6F85
P 4450 2050
F 0 "C7" H 4542 2096 50  0000 L CNN
F 1 "2.2uF" H 4542 2005 50  0000 L CNN
F 2 "" H 4450 2050 50  0001 C CNN
F 3 "~" H 4450 2050 50  0001 C CNN
	1    4450 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 2150 4350 2150
Wire Wire Line
	4600 2150 4450 2150
Connection ~ 4450 2150
$Comp
L power:GND #PWR0107
U 1 1 5C2E71B8
P 4450 1950
F 0 "#PWR0107" H 4450 1700 50  0001 C CNN
F 1 "GND" H 4455 1777 50  0000 C CNN
F 2 "" H 4450 1950 50  0001 C CNN
F 3 "" H 4450 1950 50  0001 C CNN
	1    4450 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 3600 5550 3600
Wire Wire Line
	5550 4000 5000 4000
Wire Wire Line
	5000 4000 5000 3700
Wire Wire Line
	5000 3700 4600 3700
Wire Wire Line
	5550 3800 4950 3800
Wire Wire Line
	4950 3800 4950 3900
Wire Wire Line
	4950 3900 4600 3900
$Comp
L power:+3.3V #PWR0108
U 1 1 5C2E8449
P 7200 4200
F 0 "#PWR0108" H 7200 4050 50  0001 C CNN
F 1 "+3.3V" H 7215 4373 50  0000 C CNN
F 2 "" H 7200 4200 50  0001 C CNN
F 3 "" H 7200 4200 50  0001 C CNN
	1    7200 4200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5C2E8786
P 7200 3400
F 0 "#PWR0109" H 7200 3150 50  0001 C CNN
F 1 "GND" V 7205 3272 50  0000 R CNN
F 2 "" H 7200 3400 50  0001 C CNN
F 3 "" H 7200 3400 50  0001 C CNN
	1    7200 3400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5C2E9294
P 5950 3200
F 0 "#PWR0110" H 5950 2950 50  0001 C CNN
F 1 "GND" V 5955 3072 50  0000 R CNN
F 2 "" H 5950 3200 50  0001 C CNN
F 3 "" H 5950 3200 50  0001 C CNN
	1    5950 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:Crystal_Small Y1
U 1 1 5C2E976D
P 2800 3900
F 0 "Y1" V 2754 3988 50  0000 L CNN
F 1 "16 MHz" V 2845 3988 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm" H 2800 3900 50  0001 C CNN
F 3 "~" H 2800 3900 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Fox/FC3BACALI160?qs=sGAEpiMZZMsBj6bBr9Q9abMK2mGYOCsJFYGjMJrML7h3sIdoydgaMg%3d%3d" V 2800 3900 50  0001 C CNN "SourcingLink"
	1    2800 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 4000 3200 4000
Wire Wire Line
	3200 3800 2800 3800
$Comp
L Device:C_Small C2
U 1 1 5C2E9D7F
P 2500 3750
F 0 "C2" V 2271 3750 50  0000 C CNN
F 1 "22 pF" V 2362 3750 50  0000 C CNN
F 2 "" H 2500 3750 50  0001 C CNN
F 3 "~" H 2500 3750 50  0001 C CNN
	1    2500 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5C2E9DD9
P 2500 4050
F 0 "C3" V 2750 4050 50  0000 C CNN
F 1 "22 pF" V 2650 4050 50  0000 C CNN
F 2 "" H 2500 4050 50  0001 C CNN
F 3 "~" H 2500 4050 50  0001 C CNN
	1    2500 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 3750 2800 3750
Wire Wire Line
	2800 3750 2800 3800
Connection ~ 2800 3800
Wire Wire Line
	2600 4050 2800 4050
Wire Wire Line
	2800 4050 2800 4000
Connection ~ 2800 4000
$Comp
L power:GND #PWR0111
U 1 1 5C2EA927
P 2400 3750
F 0 "#PWR0111" H 2400 3500 50  0001 C CNN
F 1 "GND" V 2405 3622 50  0000 R CNN
F 2 "" H 2400 3750 50  0001 C CNN
F 3 "" H 2400 3750 50  0001 C CNN
	1    2400 3750
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5C2EA9AC
P 2400 4050
F 0 "#PWR0112" H 2400 3800 50  0001 C CNN
F 1 "GND" V 2405 3922 50  0000 R CNN
F 2 "" H 2400 4050 50  0001 C CNN
F 3 "" H 2400 4050 50  0001 C CNN
	1    2400 4050
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0113
U 1 1 5C2EB674
P 6350 4400
F 0 "#PWR0113" H 6350 4250 50  0001 C CNN
F 1 "+3.3V" V 6365 4528 50  0000 L CNN
F 2 "" H 6350 4400 50  0001 C CNN
F 3 "" H 6350 4400 50  0001 C CNN
	1    6350 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 3300 3800 3200
Wire Wire Line
	3800 3200 3900 3200
Wire Wire Line
	4000 3200 4000 3300
Wire Wire Line
	3900 3300 3900 3200
Connection ~ 3900 3200
Wire Wire Line
	3900 3200 4000 3200
$Comp
L power:+5V #PWR0114
U 1 1 5C2EC92C
P 3900 3100
F 0 "#PWR0114" H 3900 2950 50  0001 C CNN
F 1 "+5V" H 3915 3273 50  0000 C CNN
F 2 "" H 3900 3100 50  0001 C CNN
F 3 "" H 3900 3100 50  0001 C CNN
	1    3900 3100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5C2ECBCD
P 3900 6100
F 0 "#PWR0115" H 3900 5850 50  0001 C CNN
F 1 "GND" V 3905 5972 50  0000 R CNN
F 2 "" H 3900 6100 50  0001 C CNN
F 3 "" H 3900 6100 50  0001 C CNN
	1    3900 6100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 6100 3800 6100
Connection ~ 3900 6100
Wire Wire Line
	7600 3700 7800 3700
Wire Wire Line
	7800 3700 7800 3750
Wire Wire Line
	7800 3850 7800 3900
Wire Wire Line
	7800 3900 7600 3900
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5C2EA1ED
P 8000 3750
F 0 "J3" H 8080 3742 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 8080 3651 50  0000 L CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x02_P3.50mm_Vertical" H 8000 3750 50  0001 C CNN
F 3 "~" H 8000 3750 50  0001 C CNN
	1    8000 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J1
U 1 1 5C2EB535
P 1650 4450
F 0 "J1" H 1705 4917 50  0000 C CNN
F 1 "USB_B_Micro" H 1705 4826 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 1800 4400 50  0001 C CNN
F 3 "~" H 1800 4400 50  0001 C CNN
	1    1650 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 4450 2100 4450
Wire Wire Line
	2950 4450 2950 4200
Wire Wire Line
	2950 4200 3200 4200
Wire Wire Line
	3200 4300 3050 4300
Wire Wire Line
	3050 4300 3050 4550
Wire Wire Line
	3050 4550 2300 4550
$Comp
L Device:R_Small R1
U 1 1 5C2ECA9C
P 2200 4450
F 0 "R1" V 2004 4450 50  0000 C CNN
F 1 "22" V 2095 4450 50  0000 C CNN
F 2 "" H 2200 4450 50  0001 C CNN
F 3 "~" H 2200 4450 50  0001 C CNN
	1    2200 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 4450 2950 4450
$Comp
L Device:R_Small R2
U 1 1 5C2ECD45
P 2200 4550
F 0 "R2" V 2400 4550 50  0000 C CNN
F 1 "22" V 2300 4550 50  0000 C CNN
F 2 "" H 2200 4550 50  0001 C CNN
F 3 "~" H 2200 4550 50  0001 C CNN
	1    2200 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	2100 4550 1950 4550
$Comp
L Device:C_Small C4
U 1 1 5C2ECF73
P 3150 4750
F 0 "C4" H 2950 4800 50  0000 L CNN
F 1 "1uF" H 2900 4700 50  0000 L CNN
F 2 "" H 3150 4750 50  0001 C CNN
F 3 "~" H 3150 4750 50  0001 C CNN
	1    3150 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5C2ED16E
P 3150 4850
F 0 "#PWR0116" H 3150 4600 50  0001 C CNN
F 1 "GND" H 3155 4677 50  0000 C CNN
F 2 "" H 3150 4850 50  0001 C CNN
F 3 "" H 3150 4850 50  0001 C CNN
	1    3150 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4500 3150 4500
Wire Wire Line
	3150 4500 3150 4650
Wire Wire Line
	1950 4250 2000 4250
Wire Wire Line
	2000 4250 2000 4850
$Comp
L Device:CP1_Small C1
U 1 1 5C2F01E4
P 2000 4950
F 0 "C1" H 2091 4996 50  0000 L CNN
F 1 "10uF" H 2091 4905 50  0000 L CNN
F 2 "" H 2000 4950 50  0001 C CNN
F 3 "~" H 2000 4950 50  0001 C CNN
	1    2000 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5C2F032F
P 2000 5050
F 0 "#PWR0117" H 2000 4800 50  0001 C CNN
F 1 "GND" H 2005 4877 50  0000 C CNN
F 2 "" H 2000 5050 50  0001 C CNN
F 3 "" H 2000 5050 50  0001 C CNN
	1    2000 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4850 1650 5050
Wire Wire Line
	1650 5050 2000 5050
Connection ~ 2000 5050
$Comp
L power:+5V #PWR0118
U 1 1 5C2F11DF
P 2000 4250
F 0 "#PWR0118" H 2000 4100 50  0001 C CNN
F 1 "+5V" H 2015 4423 50  0000 C CNN
F 2 "" H 2000 4250 50  0001 C CNN
F 3 "" H 2000 4250 50  0001 C CNN
	1    2000 4250
	1    0    0    -1  
$EndComp
Connection ~ 2000 4250
$Comp
L Device:C_Small C6
U 1 1 5C2EDBD5
P 4000 3100
F 0 "C6" V 3771 3100 50  0000 C CNN
F 1 "10nF" V 3862 3100 50  0000 C CNN
F 2 "" H 4000 3100 50  0001 C CNN
F 3 "~" H 4000 3100 50  0001 C CNN
	1    4000 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 3100 3900 3200
Connection ~ 3900 3100
$Comp
L power:GND #PWR0119
U 1 1 5C2EEB0A
P 4100 3100
F 0 "#PWR0119" H 4100 2850 50  0001 C CNN
F 1 "GND" V 4105 2972 50  0000 R CNN
F 2 "" H 4100 3100 50  0001 C CNN
F 3 "" H 4100 3100 50  0001 C CNN
	1    4100 3100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x16_Female J2
U 1 1 5C345998
P 6050 5700
F 0 "J2" H 6077 5676 50  0000 L CNN
F 1 "Conn_01x16_Female" H 6077 5585 50  0000 L CNN
F 2 "" H 6050 5700 50  0001 C CNN
F 3 "~" H 6050 5700 50  0001 C CNN
	1    6050 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5100 5100 5100
Wire Wire Line
	5100 5100 5100 5600
Wire Wire Line
	5100 5600 5850 5600
Wire Wire Line
	4600 5200 5050 5200
Wire Wire Line
	5050 5200 5050 5700
Wire Wire Line
	5050 5700 5850 5700
Wire Wire Line
	4600 5300 5000 5300
Wire Wire Line
	5000 5300 5000 5800
Wire Wire Line
	5000 5800 5850 5800
Wire Wire Line
	4600 5400 4950 5400
Wire Wire Line
	4950 5400 4950 5900
Wire Wire Line
	4950 5900 5850 5900
Wire Wire Line
	4600 5500 4900 5500
Wire Wire Line
	4900 5500 4900 6000
Wire Wire Line
	4900 6000 5850 6000
Wire Wire Line
	4600 5600 4850 5600
Wire Wire Line
	4850 5600 4850 6100
Wire Wire Line
	4850 6100 5850 6100
Wire Wire Line
	4600 5700 4800 5700
Wire Wire Line
	4800 5700 4800 6200
Wire Wire Line
	4800 6200 5850 6200
Wire Wire Line
	4600 5800 4750 5800
Wire Wire Line
	4750 5800 4750 6300
Wire Wire Line
	4750 6300 5850 6300
$Comp
L power:GND #PWR0120
U 1 1 5C35378E
P 5400 4950
F 0 "#PWR0120" H 5400 4700 50  0001 C CNN
F 1 "GND" H 5405 4777 50  0000 C CNN
F 2 "" H 5400 4950 50  0001 C CNN
F 3 "" H 5400 4950 50  0001 C CNN
	1    5400 4950
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0121
U 1 1 5C3537F8
P 5650 4950
F 0 "#PWR0121" H 5650 4800 50  0001 C CNN
F 1 "+5V" H 5665 5123 50  0000 C CNN
F 2 "" H 5650 4950 50  0001 C CNN
F 3 "" H 5650 4950 50  0001 C CNN
	1    5650 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4900 5150 4900
Wire Wire Line
	5150 4900 5150 5500
Wire Wire Line
	5150 5500 5850 5500
Wire Wire Line
	4600 4800 5200 4800
Wire Wire Line
	5200 4800 5200 5400
Wire Wire Line
	5200 5400 5850 5400
Wire Wire Line
	4600 4700 5250 4700
Wire Wire Line
	5250 4700 5250 5300
Wire Wire Line
	5250 5300 5850 5300
$Comp
L power:GND #PWR0122
U 1 1 5C3594B5
P 5850 6500
F 0 "#PWR0122" H 5850 6250 50  0001 C CNN
F 1 "GND" V 5855 6372 50  0000 R CNN
F 2 "" H 5850 6500 50  0001 C CNN
F 3 "" H 5850 6500 50  0001 C CNN
	1    5850 6500
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0123
U 1 1 5C359534
P 5850 6400
F 0 "#PWR0123" H 5850 6250 50  0001 C CNN
F 1 "+5V" V 5865 6528 50  0000 L CNN
F 2 "" H 5850 6400 50  0001 C CNN
F 3 "" H 5850 6400 50  0001 C CNN
	1    5850 6400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5C35962E
P 5550 5200
F 0 "R3" V 5354 5200 50  0000 C CNN
F 1 "10k" V 5445 5200 50  0000 C CNN
F 2 "" H 5550 5200 50  0001 C CNN
F 3 "~" H 5550 5200 50  0001 C CNN
	1    5550 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 4950 5400 5000
Wire Wire Line
	5400 5200 5450 5200
Wire Wire Line
	5650 5200 5750 5200
Wire Wire Line
	5850 5000 5400 5000
Connection ~ 5400 5000
Wire Wire Line
	5400 5000 5400 5200
Wire Wire Line
	5650 4950 5750 4950
Wire Wire Line
	5750 4950 5750 5100
Wire Wire Line
	5750 5100 5850 5100
Wire Wire Line
	5750 5100 5750 5200
Connection ~ 5750 5100
Connection ~ 5750 5200
Wire Wire Line
	5750 5200 5850 5200
$EndSCHEMATC
